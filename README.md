# 20190207 - Deref.rs \#3

Thu, 07. Feb. 2019

https://derefrs.connpass.com/event/118802/

```toml
[meetup]
name = "Deref Rust"
date = "20190207"
version = "0.0.3"
attendees = [
  "Yasuhiro Asaka <yasuhiro.asaka@grauwoelfchen.net>"
]
repository = "https://gitlab.com/derefrs/reading-logs/20190207-3"
keywords = ["Rust"]

[locations]
park6 = {site = "Roppongi"}
zulip = {site = "https://derefrs.zulipchat.com", optional = true }
```

## Notes

| Name | Snippet / Note / What I did in few words |
|--|--|
| @grauwoelfchen | Read Chapter 2. "Functional Control Flow" of a book [Hands-On Functional Programming in Rust](https://www.packtpub.com/application-development/hands-functional-programming-rust) (978-1-78883-935-8) |


## Links

* [Deref Rust - GitLab.com](https://gitlab.com/derefrs)
* [Deref Rust - Zulip Chat](https://derefrs.zulipchat.com/)

## License

`MIT`

```text
Reading Logs
Copyright (c) 2019 Deref.rs

This is free software: You can redistribute it and/or modify
it under the terms of the MIT License.
```
